godice
A down and dirty dice rolling routines for gaming.

// Roll `count` number of `sides` dice and returns the total.
// Commonly shown as 3d6. Three six sided dice.
func Roll(count, sides int) int

// Roll `count` number of `sides` dice and returns a slice containing the individual rolls.
// Commonly shown as 3d6. Three six sided dice.
func RollKeep(count, sides int) []int

// Roll `count` number of `sides` dice, keeping the top `keep` sided and returns the total.
// Commonly shown as 4d6k3. Four six sided dice keeping the top three.
func RollTop(count, sides, keep int) int

// Rollfor72 Will roll `Roll(3,d)` until total of 6 rolls is 72 or greater. Returns slice with results.
func Rollfor72() []int

Update: Moving to bitbucket

Example

```golang
package main

import (
  "github.com/misterunix/godice"
  "fmt"
)

func main() {

  var results []int

  results = append(results,godice.Roll(1,4))
  results = append(results,godice.Roll(1,6))
  results = append(results,godice.Roll(1,8))
  results = append(results,godice.Roll(1,10))
  results = append(results,godice.Roll(1,12))
  results = append(results,godice.Roll(1,20))
  fmt.Println("Dice 4,6,8,10,12,20: ",results)
  results = nil

  dierolls := godice.RollKeep(10,6)
  fmt.Println( "Dice 6x10: ", dierolls )

  for i:=0;i<10;i++ {
    results = append(results,godice.RollTop(4,6,3))
  }
  fmt.Println( "Dice 4d6k3 x 10:", results )

  var r int
  for i:=0;i<1000000;i++ {
    r=r+godice.Roll(1,6)
  }
  ar := float64(r) / 1000000.0
  fmt.Println("1M rolls of d6 averaged: ",ar)
}
```